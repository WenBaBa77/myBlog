package com.wen.blog.persistence.beans;

import com.wen.blog.framework.object.AbstractDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
public class SysUpdateRecorde extends AbstractDO {
    private String version;
    private String description;
    private Date recordeTime;
}
