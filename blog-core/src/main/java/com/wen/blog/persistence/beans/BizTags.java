package com.wen.blog.persistence.beans;

import com.wen.blog.framework.object.AbstractDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BizTags extends AbstractDO {
    private String name;
    private String description;
}
