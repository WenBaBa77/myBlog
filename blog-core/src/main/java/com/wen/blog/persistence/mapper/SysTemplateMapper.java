package com.wen.blog.persistence.mapper;

import com.wen.blog.business.vo.TemplateConditionVO;
import com.wen.blog.persistence.beans.SysTemplate;
import com.wen.blog.plugin.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
@Repository
public interface SysTemplateMapper extends BaseMapper<SysTemplate> {

    /**
     * 分页查询
     *
     * @param vo
     * @return
     */
    List<SysTemplate> findPageBreakByCondition(TemplateConditionVO vo);
}
