package com.wen.blog.persistence.mapper;

import com.wen.blog.business.vo.LogConditionVO;
import com.wen.blog.persistence.beans.SysLog;
import com.wen.blog.plugin.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/01/09 17:45
 * @since 1.0
 */
@Repository
public interface SysLogMapper extends BaseMapper<SysLog> {

    /**
     * 分页查询
     *
     * @param vo
     * @return
     */
    List<SysLog> findPageBreakByCondition(LogConditionVO vo);
}
