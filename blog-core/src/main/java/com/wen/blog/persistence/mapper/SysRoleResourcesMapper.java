package com.wen.blog.persistence.mapper;

import com.wen.blog.persistence.beans.SysRoleResources;
import com.wen.blog.plugin.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
@Repository
public interface SysRoleResourcesMapper extends BaseMapper<SysRoleResources> {
}
