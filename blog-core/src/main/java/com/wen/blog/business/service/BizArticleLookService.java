package com.wen.blog.business.service;


import com.wen.blog.business.entity.ArticleLook;

/**
 * 文章浏览记录
 *
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public interface BizArticleLookService {

    ArticleLook insert(ArticleLook articleLook);
}
