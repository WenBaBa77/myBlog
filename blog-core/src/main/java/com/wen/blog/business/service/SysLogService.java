package com.wen.blog.business.service;


import com.github.pagehelper.PageInfo;
import com.wen.blog.business.entity.Log;
import com.wen.blog.business.enums.PlatformEnum;
import com.wen.blog.business.vo.LogConditionVO;
import com.wen.blog.framework.object.AbstractService;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/01/09 17:40
 * @since 1.0
 */
public interface SysLogService extends AbstractService<Log, Integer> {

    /**
     * 分页查询
     *
     * @param vo
     * @return
     */
    PageInfo<Log> findPageBreakByCondition(LogConditionVO vo);

    void asyncSaveSystemLog(PlatformEnum platform, String bussinessName);
}
