package com.wen.blog.business.enums;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public enum BaiduPushTypeEnum {
    // urls: 推送, update: 更新, del: 删除
    urls, update, del
}
