package com.wen.blog.business.vo;

import com.wen.blog.framework.object.BaseConditionVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CommentConditionVO extends BaseConditionVO {
    private Long userId;
    private Long sid;
    private Long pid;
    private String qq;
    private String email;
    private String url;
    private String status;
}

