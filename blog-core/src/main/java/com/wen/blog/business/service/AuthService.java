package com.wen.blog.business.service;

/**
 * @author wenchun
 * @version 1.0
 * @date 2019/5/25 14:32
 * @since 1.8
 */
public interface AuthService {

    boolean login(String source, String code, String auth_code);

    boolean revoke(String source, Long userId);

    void logout();
}
