package com.wen.blog.business.enums;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/5/7 15:16
 * @since 1.0
 */
public enum PlatformEnum {
    ADMIN, WEB
}
