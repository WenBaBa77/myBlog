package com.wen.blog.business.enums;

/**
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public enum LogLevelEnum {
    ERROR, WARN, INFO, DEBUG
}
