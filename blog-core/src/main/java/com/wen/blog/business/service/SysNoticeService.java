package com.wen.blog.business.service;


import com.github.pagehelper.PageInfo;
import com.wen.blog.business.dto.SysNoticeDTO;
import com.wen.blog.business.entity.Notice;
import com.wen.blog.business.vo.NoticeConditionVO;
import com.wen.blog.framework.object.AbstractService;

import java.util.List;

/**
 * 系统通知
 *
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public interface SysNoticeService extends AbstractService<Notice, Long> {

    /**
     * 分页查询
     *
     * @param vo
     * @return
     */
    PageInfo<Notice> findPageBreakByCondition(NoticeConditionVO vo);

    /**
     * 获取已发布的通知列表
     *
     * @return
     */
    List<SysNoticeDTO> listRelease();
}
