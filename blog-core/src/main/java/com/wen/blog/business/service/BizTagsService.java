package com.wen.blog.business.service;


import com.github.pagehelper.PageInfo;
import com.wen.blog.business.entity.Tags;
import com.wen.blog.business.vo.TagsConditionVO;
import com.wen.blog.framework.object.AbstractService;

/**
 * 标签
 *
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public interface BizTagsService extends AbstractService<Tags, Long> {

    PageInfo<Tags> findPageBreakByCondition(TagsConditionVO vo);

    Tags getByName(String name);
}
