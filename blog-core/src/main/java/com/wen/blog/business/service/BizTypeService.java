package com.wen.blog.business.service;


import com.wen.blog.framework.object.AbstractService;
import com.wen.blog.business.entity.Type;
import com.wen.blog.business.vo.TypeConditionVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * 分类
 *
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public interface BizTypeService extends AbstractService<Type, Long> {

    /**
     * 分页查询
     *
     * @param vo
     * @return
     */
    PageInfo<Type> findPageBreakByCondition(TypeConditionVO vo);

    List<Type> listParent();

    List<Type> listTypeForMenu();
}
