package com.wen.blog.business.service;


import com.wen.blog.business.entity.RoleResources;
import com.wen.blog.framework.object.AbstractService;

/**
 * 角色资源
 *
 * @author wenchun
 * @version 1.0
 * @date 2018/4/16 16:26
 * @since 1.0
 */
public interface SysRoleResourcesService extends AbstractService<RoleResources, Long> {

    /**
     * 添加角色资源
     *
     * @param roleId
     * @param resourcesId
     */
    void addRoleResources(Long roleId, String resourcesId);

    /**
     * 通过角色id批量删除
     *
     * @param roleId
     */
    void removeByRoleId(Long roleId);
}
